from threading import Thread
import urllib2
from time import time

class GetUrlThread(Thread):

	def __init__(self, url):

		self.url = url
		super(GetUrlThread, self).__init__()

	def run(self):
		resp = urllib2.urlopen(self.url)
		print self.url, resp.getcode()


def get_responses():

	urls = ['http://www.google.com', 'http://www.amazon.com', 
	'http://www.ebay.com', 'http://www.alibaba.com', 'http://www.reddit.com']

	start = time()
	threads = []

	for url in urls:
		t = GetUrlThread(url)
		threads.append(t)
		t.start()

	for t in threads:
		t.join()

	print 'Elapsed time %s' % (time() - start)


if __name__ == '__main__':

	get_responses()

"""
Explanation:
Appreciate the improvement in running time of this program.
We wrote a multi threaded program to decrease processor's idle time. While waiting for response of a particular thread's url, 
processor can work on some other thread and fetch the other thread's url.
We wanted one thread to act on one url, so overridden the constructor of thread class to pass it a url.
Execution of a thread means execution of a thread's run().
So, whatever we want the thread to do must go in its run().
Created one thread for each url and called start() on it. This tells the processor that it can execute the particular thread i.e 
the run() of thread.
We don't want the elapsed time to be evaluated until all the threads have executed, join() comes in picture here.
Calling join() on a thread tells the main thread to wait for this particular thread to finish before the main thread can execute 
the next instruction.
We call join() on all the threads, so elapsed time will be printed only after all the threads have run.

Few things about threads
Processor might not execute run() of a thread immediately after start().
You can't say in which order run() of different threads will be called.
For a specific thread, it's guaranteed that the statements inside run() will be executed sequentially.
It means that first the the url associated with the thread will be fetched and only then the recieved response will be printed.
"""