from threading import Thread
import time


class CreateThreadList(Thread):

	def run(self):
		self.entries = []

		for i in range(10):
			time.sleep(1)
			self.entries.append(i)

		print self.entries



def use_create_list_thread():

	for i in range(3):
		t = CreateThreadList()
		t.start()


if __name__ == '__main__':

	use_create_list_thread()


"""
Possibly the entries of one thread was getting printed and during this operation, processor switched to some other 
thread and started printing the entries for other thread. 
We want to ensure that entries get printed one after another for separate threads.

"""