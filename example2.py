from threading import Thread


# define a global variable:

some_var = 0

class IncrementThread(Thread):

	def run(self):

		# we want to read a global variable  
		# and then increment it:

		global some_var
		read_value = some_var
		print 'some_var in %s in %d ' % (self.name, read_value)

		some_var = read_value + 1
		
		print 'some_var in %s after increment  %d ' % (self.name, read_value)


def use_increment_thread():

	threads = []

	for i in range(50):
		t = IncrementThread()
		threads.append(t)
		t.start()

	for t in threads:
		t.join()

	print "After 50 modifications, some_var should have become 50"
	print "After 50 modifications, some_var is %d" % (some_var,)


if __name__ == '__main__':
	use_increment_thread()


"""
Explanation:
There is a global variable and all the threads will modify it.
All threads should add 1 to the existing value of the variable.
There are 50 threads, so at end the value of some_var should become 50, but it doesn't.
Why some_var didn't reach 50?
At some point thread t1 read the value of some_var as 15 and then processor took the control from this thread 
and gave it to thread t2.
t2 also reads some_var as 15.
Both t1 and t2 reset the value of some_var to 15+1 i.e 16.
But when two threads act on some_var we expected it's value to be increased by 2.
So, we have a race condition here. (http://en.wikipedia.org/wiki/Race_condition#Example)
A similar race condition might have occurred few more times and so value of some_var at end remains something like 41 or 42 
or anything less than 50.
"""