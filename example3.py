from threading import Thread, Lock


lock = Lock()

# define a global variable:

some_var = 0

class IncrementThread(Thread):

	def run(self):

		# we want to read a global variable  
		# and then increment it:

		global some_var
		lock.acquire()
		read_value = some_var
		print 'some_var in %s in %d ' % (self.name, read_value)

		some_var = read_value + 1
		
		print 'some_var in %s after increment  %d ' % (self.name, read_value)

		lock.release()


def use_increment_thread():

	threads = []

	for i in range(50):
		t = IncrementThread()
		threads.append(t)
		t.start()

	for t in threads:
		t.join()

	print "After 50 modifications, some_var should have become 50"
	print "After 50 modifications, some_var is %d" % (some_var,)


if __name__ == '__main__':
	use_increment_thread()


"""
Explanation:

Lock is used to guard against race condition.
If thread t1 has acquired the lock before performing a set of operations, no other thread can perform the same set of operation 
until t1 releases the lock.
We want to make sure that once t1 has read some_var, no other thread can read some_var until t1 is done with modifying the value of 
some_var.
So reading some_var and modifying it are logically related operations here.
And that is why we keep read and modify part of some_var guarded by a Lock instance.
Lock is a separate object and it will be acquired by the thread from whose context it is called.

"""